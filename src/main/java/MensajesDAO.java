import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MensajesDAO {

    private static ArrayList<Mensajes> listaMensajes = new ArrayList<>();

    public static void crearMensajeDB(Mensajes mensaje) {
        Conexion dbConnect = Conexion.getInstance();

        try (Connection conexion = dbConnect.getConnection()) {

            // Inserción de los datos
            PreparedStatement ps = null;
            try {
                String query = "INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?,?);";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                ps.executeUpdate();
                System.out.println("Mensaje creado");
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static ArrayList<Mensajes> leerMensajesDB() {
        Conexion dbConnect = Conexion.getInstance();

        try (Connection conexion = dbConnect.getConnection()) {

            // Lectura de la base de datos
            PreparedStatement ps = null;
            ResultSet rs = null;

            try {
                String query = "SELECT * FROM mensajes";
                ps = conexion.prepareStatement(query);
                rs = ps.executeQuery();

                // limpiar la lista
                listaMensajes.clear();

                // Agregar los registros a la lista de mensajes
                while (rs.next()) {
                    listaMensajes.add(new Mensajes(
                            rs.getInt("id_mensaje"),
                            rs.getString("mensaje"),
                            rs.getString("autor_mensaje"),
                            rs.getString("fecha_mensaje")
                    ));
                }
            } catch (SQLException ex) {
                System.out.println("No se pudieron recuperar los mensajes");
                System.out.println(ex);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return listaMensajes;
    }

    public static void borrarMensajeDB(int id_mensaje) {
        Conexion dbConnect = Conexion.getInstance();

        try (Connection conexion = dbConnect.getConnection()) {

            PreparedStatement ps = null;

            try {
                String query = "DELETE FROM mensajes WHERE id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setInt(1, id_mensaje);

                // Validar si se encontró el id que se desea eliminar
                int countRowsUpdated = ps.executeUpdate();
                if (countRowsUpdated != 0) {
                    System.out.println("El mensaje ha sido borrado");
                } else {
                    System.out.println("No se encontró el id ingresado.");
                }

            } catch (SQLException ex) {
                System.out.println("No se pudieron recuperar los mensajes");
                System.out.println(ex);
            }

        } catch (SQLException e) {
            System.out.println(e);
            System.out.println("No se pudo borrar el mensaje");
        }
    }

    public static void actualizarMensajeDB(Mensajes mensajes) {
        Conexion dbConnect = Conexion.getInstance();

        try (Connection conexion = dbConnect.getConnection()) {

            PreparedStatement ps = null;

            try {
                String query = "UPDATE mensajes SET mensaje = ? WHERE id_mensaje = ?;";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensajes.getMensaje());
                ps.setInt(2, mensajes.getId_mensaje());

                // Validar si se encontró el id que se desea actualizar
                int countRowsUpdated = ps.executeUpdate();
                if (countRowsUpdated != 0) {
                    System.out.println("El mensaje se actualizó correctamente");
                } else {
                    System.out.println("No se encontró el id ingresado.");
                }

            } catch (SQLException ex) {
                System.out.println(ex);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}
