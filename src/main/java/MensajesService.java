import java.util.ArrayList;
import java.util.Scanner;

public class MensajesService {

    public static void crearMensaje() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu mensaje");

        // Leer el mensaje del usuario
        String mensaje = sc.nextLine();

        System.out.println("Tu nombre");
        String nombre = sc.nextLine();

        Mensajes registro = new Mensajes();
        registro.setMensaje(mensaje);
        registro.setAutor_mensaje(nombre);

        // Enviar la información a MensajesDAO
        MensajesDAO.crearMensajeDB(registro);
    }

    public static void listarMensajes() {
        // Leer la información
        ArrayList<Mensajes> mensajes = MensajesDAO.leerMensajesDB();

        for (Mensajes msg : mensajes) {
            System.out.println("Mensaje: " + msg.getMensaje());
            System.out.println("Autor: " + msg.getAutor_mensaje());
            System.out.println("Mensaje: " + msg.getFecha_mensaje());
            System.out.println();
        }
    }

    public static void borrarMensaje() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Indica el ID del mensaje a borrar");
        int idMensaje = sc.nextInt();
        MensajesDAO.borrarMensajeDB(idMensaje);
    }

    public static void editarMensaje() {
        Scanner sc = new Scanner(System.in);

        // Solicitar al usuario el mensaje nuevo
        System.out.println("Escribe tu nuevo mensaje");
        String mensaje = sc.nextLine();

        // Solicitar al usuario el id a editar
        System.out.println("Escribe el ID del mensaje que deseas editar");
        int idMensaje = sc.nextInt();

        // Nuevo objeto Mensaje para establecer los valores solicitados
        Mensajes actualizacion = new Mensajes();
        actualizacion.setId_mensaje(idMensaje);
        actualizacion.setMensaje(mensaje);

        MensajesDAO.actualizarMensajeDB(actualizacion);
    }
}
