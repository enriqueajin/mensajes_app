import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    private static Conexion conexionInstance = null;

    public static Conexion getInstance() {
        if (conexionInstance == null) {
            conexionInstance = new Conexion();
        }
        return conexionInstance;
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/mensajes_app",
                    "root",
                    "");

        } catch (SQLException e) {
            System.out.println(e);
        }
        return connection;
    }
}
